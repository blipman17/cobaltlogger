﻿using System;
using System.Diagnostics;
using System.Threading;
using CobaltLogger;
using Timer = System.Timers.Timer;

namespace CobaltLogger.Runner
{
    class Program
    {
        public string str = "JustSomeStringData";

        static void Main(string[] args)
        {
            new Program();

        }

        public Program()
        {
            var logSettings = new LogSettings(
                new PostgresDatabaseSettings(
                    admin: new DatabaseUser("postgres", "pwd"),
                    user: new DatabaseUser("postgres", "pwd"),
                    database: "logging",
                    host: "127.0.0.1",
                    logTable: "Log",
                    port: "5432"
                ), sequentialFileLogSettings: new SequentialFileLogSettings[]
                {
                    new SequentialFileLogSettings(
                        directory: LogSettings.DefaultSequentialLogFileDir,
                        file: $"{LogSettings.CurrentTime.ToString().RemoveIllegalFileCharacters()}.Log",
                        logTypes: new[] { LogType.Info, LogType.Warning, LogType.Exception },
                        printStackTrace: false,
                        showUtcTime: true,
                        showLocalTime: true,
                        printObject: false)},
                eventFileLogSettings: new EventFileLogSettings[]
                {
                    new EventFileLogSettings(
                        directory: LogSettings.DefaultEventLogFileDir,
                        logTypes: new[] { LogType.Exception },
                        printStackTrace: true,
                        showUtcTime: true,
                        showLocalTime: true,
                        printObject: true)
                });

            Log.Initialize(logSettings);

            //Log.Info("Hello log world!", this);

            try
            {
                throw new Exception();
            }
            catch (Exception e)
            {
                Log.Exception(e, "Damn... something broke", this);
            }

            TimeSpan totalElapsed = new TimeSpan(0);
            for (int k = 0; k < 10; k++)
            {
                Console.WriteLine("Starting new mesurement");
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                for (int i = 0; i < 10000; i++)
                {
                    try
                    {
                        throw new Exception();
                    }
                    catch (Exception e)
                    {
                        Log.Exception(e, "Damn... something broke", this);
                    }
                }
                stopwatch.Stop();
                var elapsed = stopwatch.Elapsed;
                totalElapsed += elapsed;
                Console.WriteLine(elapsed.ToString());

                Thread.Sleep(elapsed);
            }

            Console.WriteLine($"avg time: {new TimeSpan(totalElapsed.Ticks / 10)}");

            Thread.Sleep(1000);

            Log.Shutdown();
        }
    }
}
