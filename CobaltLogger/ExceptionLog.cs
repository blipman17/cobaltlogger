﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CobaltLogger
{
    internal struct ExceptionLog
    {
        public readonly Exception Exception;
        public readonly object obj;

        public ExceptionLog(Exception exception, object obj)
        {
            Exception = exception;
            this.obj = obj;
        }
    }
}
