﻿namespace CobaltLogger
{
    public enum LogType
    {
        Debug = 0,
        Verbose = 1,
        Exception = 2,
        Warning = 3,
        Info = 4
    }
}