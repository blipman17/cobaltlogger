﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.Win32;
using Newtonsoft.Json;
using Npgsql;

namespace CobaltLogger
{
    public class PostgresDatabaseSettings
    {
        public readonly string Database;
        public readonly string Host;

        public readonly DatabaseUser Admin;
        public readonly DatabaseUser User;

        public readonly string LogTable;
        public readonly string Port;

        public PostgresDatabaseSettings(DatabaseUser admin, DatabaseUser user, string database, string host, string logTable, string port)
        {
            Admin = admin;
            User = user;
            Database = database;
            Host = host;
            LogTable = logTable;
            Port = port;
        }

        public void InitializeDatabase()
        {
            if (string.IsNullOrWhiteSpace(Admin.User) || string.IsNullOrWhiteSpace(Database))
            {
                throw new ArgumentException("Admin username and database must be set prior to creating it.");
            }

            /*
            using (var connection = new NpgsqlConnection(User.GetConnectionString(this)))
            {
                connection.Open();
                connection.Close();
            }
            */
        }
    }
}