using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;

namespace CobaltLogger
{
    internal class LogWriter : IDisposable
    {
        private readonly Dictionary<FileLogSettings, Task> fileTasks = new Dictionary<FileLogSettings, Task>();
        public Task[] Tasks { get { return fileTasks.Values.ToArray(); } }

        private readonly object fileQueuesLock = new object();
        private readonly Dictionary<FileLogSettings, BlockingCollection<LogWriterWork>> fileQueues = new Dictionary<FileLogSettings, BlockingCollection<LogWriterWork>>();

        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private Task databaseTask;
        private readonly BlockingCollection<LogEntry> databaseWork = new BlockingCollection<LogEntry>();

        private void Write(LogWriterWork logWriterWork)
        {
            BlockingCollection<LogWriterWork> taskQueue;
            bool hasFileQueue;
            lock (fileQueuesLock)
            {
                hasFileQueue = !fileQueues.TryGetValue(logWriterWork.Setting, out taskQueue);
            }

            if (hasFileQueue)
            {
                taskQueue = AddNewTaskQueue(logWriterWork.Setting);
            }

            taskQueue.Add(logWriterWork);

            Task fileTask;
            if (!fileTasks.TryGetValue(logWriterWork.Setting, out fileTask)
                || fileTask.IsCompleted
                || fileTask.IsCanceled
                || fileTask.IsFaulted)
            {
                AddNewTaskExecutor(logWriterWork.Setting);
            }
        }

        private BlockingCollection<LogWriterWork> AddNewTaskQueue(FileLogSettings fileLogSettings)
        {
            var blockingCollection = new BlockingCollection<LogWriterWork>();
            lock (fileQueuesLock)
            {
                fileQueues[fileLogSettings] = blockingCollection;
            }
            return blockingCollection;
        }

        private void AddNewTaskExecutor(FileLogSettings fileLogSettings)
        {
            var task = new Task(() => TaskExecutorWork(fileLogSettings, cancellationTokenSource.Token), cancellationTokenSource.Token);
            fileTasks[fileLogSettings] = task;
            task.Start();
        }

        private void TaskExecutorWork(FileLogSettings fileLogSettings, CancellationToken token)
        {
            BlockingCollection<LogWriterWork> queue;
            lock (fileQueuesLock)
            {
                queue = fileQueues[fileLogSettings];
            }

            while (!token.IsCancellationRequested)
            {
                LogWriterWork work;
                if (!queue.TryTake(out work, TimeSpan.FromSeconds(1).Milliseconds, token))
                {
                    // There's no work to be done. Free os resources.
                    return;
                }
                var jObj = Format(work);
                var removeIllegalPathCharacters = work.Setting.LogFile.RemoveIllegalCharacters();
                File.AppendAllText(removeIllegalPathCharacters, $"{jObj.ToString(work.Formatting)}\r\n", Encoding.UTF8);
            }
        }

        public JObject Format(LogWriterWork logWriterWork)
        {
            var content = new JObject();
            if (logWriterWork.Setting.ShowUtcTime)
            {
                content["Utc Time"] = logWriterWork.Entry.Instant.InUtc().ToString();
            }
            if (logWriterWork.Setting.ShowLocalTime)
            {
                content["At location Time"] = logWriterWork.Entry.Instant.InZone(DateTimeZoneProviders.Bcl.GetSystemDefault()).LocalDateTime.ToString();
            }
            content["message"] = logWriterWork.Entry.Message;
            if (logWriterWork.Setting.PrintObject && logWriterWork.Entry.JObject != null)
            {
                content["reference object"] = JsonConvert.SerializeObject(logWriterWork.Entry.JObject);
            }
            if (logWriterWork.Setting.PrintStackTrace)
            {
                AddStackTrace(content, logWriterWork.Entry);
            }
            return content;
        }

        private void AddStackTrace(JObject content, LogEntry entry)
        {
            var originalStackFrames = entry.StackTrace.GetFrames();
            if (originalStackFrames == null)
            {
                return;
            }

            var newStackFrames = new List<string>(originalStackFrames.Length);

            foreach (var stackFrame in originalStackFrames)
            {
                if (!stackFrame.GetMethod()?.DeclaringType?.Namespace?.Equals(nameof(CobaltLogger)) ?? false)
                {
                    newStackFrames.Add(stackFrame.ToString());
                }
            }

            content["Location"] = new JArray(newStackFrames);
        }

        public void Dispose()
        {
            cancellationTokenSource?.Cancel();
            cancellationTokenSource?.Dispose();
        }

        private void WriteToDatabase(LogEntry entry)
        {
            var databaseSettings = entry.Settings.PostgresDatabaseSettings;
            /*
            try
            {
                using (var connection = new NpgsqlConnection(postgresDatabaseSettings.User.GetConnectionString(postgresDatabaseSettings)))
                using (var cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = $"INSERT INTO \"{postgresDatabaseSettings.LogTable}\" (log_type, instant, message, data, stack_trace)" +
                                      " VALUES (@log_type, @instant, @message, @data, @stack_trace)";
                    AddParameters(cmd, entry, postgresDatabaseSettings);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (NpgsqlException ex)
            {
                Console.WriteLine(ex);
            }
            */
        }

        private void DatabaseWorkAction(object token)
        {
            var cancellationToken = (CancellationToken)token;
            try
            {

                while (!cancellationToken.IsCancellationRequested)
                {
                    LogEntry logEntry;
                    if (databaseWork.TryTake(out logEntry, TimeSpan.FromSeconds(1).Milliseconds, cancellationToken))
                    {
                        WriteToDatabase(logEntry);
                    }
                    else
                    {
                        // There's no work to be done. Free os resources.
                        return;
                    }
                }

            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void ProcessEvent(LogEntry entry)
        {
            entry.Settings.EventFileLogSettings.ToList()
                .Where(setting => setting.LogTypes.Contains(entry.LogType)).ToList()
                .ForEach(setting =>
                Write(new LogWriterWork(entry, setting, Formatting.Indented)));
        }

        public void ProcessSequentially(LogEntry entry)
        {
            entry.Settings.SequentialFileLogSettings.ToList()
                .Where(setting => setting.LogTypes.Contains(entry.LogType)).ToList()
                .ForEach(setting =>
                Write(new LogWriterWork(entry, setting, Formatting.None)));
        }

        public void ProcessToDatabase(LogEntry entry)
        {
            databaseWork.Add(entry);

            if (databaseTask == null || databaseTask.IsCompleted || databaseTask.IsFaulted || databaseTask.IsCanceled)
            {
                var cancellationToken = cancellationTokenSource.Token;
                var factory = new TaskFactory(cancellationToken);
                databaseTask = factory.StartNew(DatabaseWorkAction, cancellationTokenSource.Token);
            }
        }
    }
}