using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Concurrent;

namespace CobaltLogger
{
    internal class LogWriterWork
    {
        public LogWriterWork(LogEntry entry, FileLogSettings setting, Formatting formatting)
        {
            Entry = entry;
            Setting = setting;
            Formatting = formatting;
        }

        public readonly LogEntry Entry;
        public readonly FileLogSettings Setting;
        public readonly Formatting Formatting;
    }
}