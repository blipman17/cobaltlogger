﻿using Newtonsoft.Json;

namespace CobaltLogger
{
    public class SequentialFileLogSettings : FileLogSettings
    {
        private readonly string directory;
        private readonly string file;

        [JsonIgnore]
        public override string LogFile => directory + file;

        public SequentialFileLogSettings(string directory, string file, LogType[] logTypes, bool printStackTrace, bool showUtcTime, bool showLocalTime, bool printObject)
            : base(directory, logTypes, printStackTrace, showUtcTime, showLocalTime, printObject)
        {
            this.directory = directory;
            this.file = file;
        }
    }
}