﻿using System.Runtime.InteropServices.WindowsRuntime;

namespace CobaltLogger
{
    public class DatabaseUser
    {
        public readonly string User;
        private readonly string Password;

        public DatabaseUser(string user, string password)
        {
            User = user;
            Password = password;
        }

        public string GetConnectionString(PostgresDatabaseSettings settings)
        {
            return $"User ID={User};Password={Password};Host={settings.Host};port={settings.Port};Database={settings.Database};";
        }

        public string GetConnectionStringWithoutDatabase(PostgresDatabaseSettings settings)
        {
            return $"User ID={User};Password={Password};Host={settings.Host};port={settings.Port};";
        }
    }
}