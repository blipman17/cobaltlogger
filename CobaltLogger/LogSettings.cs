﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using NodaTime;

namespace CobaltLogger
{
    public class LogSettings
    {
        private static readonly ReaderWriterLockSlim LoggerSettingsLock =
            new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        private static LogSettings LOG_SETTINGS;

        public static readonly string DefaultEventLogFileDir =
            $"{Directory.GetCurrentDirectory()}\\logs\\event\\";

        public static readonly string DefaultSequentialLogFileDir =
            $"{Directory.GetCurrentDirectory()}\\logs\\sequential\\";

        public static readonly ZonedDateTime CurrentTime = GetCurrentZoneDateTime();

        public static LogSettings GlobalLogSettings
        {
            get
            {
                LoggerSettingsLock.EnterReadLock();
                try
                {
                    return LOG_SETTINGS;
                }
                finally
                {
                    LoggerSettingsLock.ExitReadLock();
                }
            }
            set
            {
                LoggerSettingsLock.EnterWriteLock();
                try
                {
                    LOG_SETTINGS = value;
                }
                finally
                {
                    LoggerSettingsLock.ExitWriteLock();
                }
            }
        }

        public readonly PostgresDatabaseSettings PostgresDatabaseSettings;

        public readonly SequentialFileLogSettings[] SequentialFileLogSettings;

        public readonly EventFileLogSettings[] EventFileLogSettings;

        public LogSettings(PostgresDatabaseSettings postgresDatabaseSettings, SequentialFileLogSettings[] sequentialFileLogSettings, EventFileLogSettings[] eventFileLogSettings)
        {
            PostgresDatabaseSettings = postgresDatabaseSettings;
            SequentialFileLogSettings = sequentialFileLogSettings;
            EventFileLogSettings = eventFileLogSettings;
        }

        public static ZonedDateTime GetCurrentZoneDateTime()
        {
            var currentInstant = SystemClock.Instance.GetCurrentInstant();
            var currentTimeZone = DateTimeZoneProviders.Bcl.GetSystemDefault();
            return currentInstant.InZone(currentTimeZone);
        }
    }
}