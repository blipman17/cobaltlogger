﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;

namespace CobaltLogger
{
    public static class Log
    {
        private static readonly LogWriter LogWriter = new LogWriter();

        public static void Initialize(LogSettings logSettings)
        {
            LogSettings.GlobalLogSettings = logSettings;

            CreateDirsIfNeeded(logSettings);

            LogSettings.GlobalLogSettings.PostgresDatabaseSettings.InitializeDatabase();
        }

        private static void CreateDirsIfNeeded(LogSettings logSettings)
        {
            logSettings.EventFileLogSettings?.ToList()?.ForEach(
                setting =>
                {
                    if (!Directory.Exists(setting.Directory))
                    {
                        Directory.CreateDirectory(setting.Directory);
                    }
                });

            logSettings.SequentialFileLogSettings?.ToList()?.ForEach(
                setting =>
                {
                    if (!Directory.Exists(setting.Directory))
                    {
                        Directory.CreateDirectory(setting.Directory);
                    }
                });
        }

        public static void Debug(string message, object obj = null)
        {
            EnqueueLog(LogType.Debug, message, obj);
        }

        public static void Verbose(string message, object obj = null)
        {
            EnqueueLog(LogType.Verbose, message, obj);
        }

        public static void Info(string message, object obj = null)
        {
            EnqueueLog(LogType.Info, message, obj);
        }

        public static void Warning(string message, object obj = null)
        {
            EnqueueLog(LogType.Warning, message, obj);
        }

        public static void Exception(Exception exception, string message = null, object obj = null)
        {
            EnqueueLog(LogType.Exception, message, new ExceptionLog(exception, obj));
        }

        private static void EnqueueLog(LogType logType, string message, object obj)
        {
            var entry = new LogEntry(message, obj, logType, LogSettings.GlobalLogSettings);

            LogWriter.ProcessSequentially(entry);
            LogWriter.ProcessEvent(entry);
            LogWriter.ProcessToDatabase(entry);
        }

        public static void Shutdown()
        {
            LogWriter.Dispose();
            Task.WaitAll(LogWriter.Tasks, TimeSpan.FromSeconds(2));
        }

    }
}
