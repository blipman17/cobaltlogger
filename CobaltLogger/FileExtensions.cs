using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CobaltLogger
{
    public static class FileExtensions
    {
        private static readonly char[] illegalFileCharacters = Path.GetInvalidFileNameChars();
        private static readonly char[] IllegalDirectoryCharacters = Path.GetInvalidPathChars();
        private static readonly char[] IllegalCharacters = (new string(illegalFileCharacters) + new string(IllegalDirectoryCharacters)).ToCharArray();

        public static string RemoveIllegalCharacters(this string path)
        {
            var i = path.LastIndexOf("\\");

            if (i != -1)
            {
                string returnValue = path.Substring(0, i + 1).RemoveIllegalDirectoryCharacters();
                returnValue += path.Substring(i + 1).RemoveIllegalFileCharacters();
                return returnValue;
            }
            else
            {
                return path.RemoveIllegalFileCharacters();
            }

        }

        public static string RemoveIllegalDirectoryCharacters(this string Directory)
        {
            return RemoveCharacters(Directory, IllegalDirectoryCharacters);
        }

        public static string RemoveIllegalFileCharacters(this string File)
        {
            return RemoveCharacters(File, illegalFileCharacters);
        }


        private static string RemoveCharacters(string path, char[] mask)
        {
            foreach (var illegalChar in mask)
            {
                path = path.Replace(illegalChar, '-');
            }
            return path;
        }
    }
}