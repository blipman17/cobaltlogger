﻿using System;
using Newtonsoft.Json;

namespace CobaltLogger
{
    public abstract class FileLogSettings
    {
        [JsonIgnore]
        public abstract string LogFile { get; }

        public readonly string Directory;

        public readonly LogType[] LogTypes;
        public readonly bool PrintStackTrace;
        public readonly bool ShowUtcTime;
        public readonly bool ShowLocalTime;
        public readonly bool PrintObject;

        public FileLogSettings(string directory, LogType[] logTypes, bool printStackTrace, bool showUtcTime, bool showLocalTime, bool printObject)
        {
            Directory = directory;
            LogTypes = logTypes;
            PrintStackTrace = printStackTrace;
            ShowUtcTime = showUtcTime;
            ShowLocalTime = showLocalTime;
            PrintObject = printObject;
        }
    }
}
