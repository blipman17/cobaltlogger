﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;

namespace CobaltLogger
{
    internal struct LogEntry
    {
        public readonly string Message;
        public readonly JObject JObject;
        public readonly LogType LogType;
        public readonly LogSettings Settings;
        public readonly Instant Instant;
        public readonly StackTrace StackTrace;

        internal LogEntry(string message, object jObject, LogType logType, LogSettings settings)
        {
            Message = message;
            JObject = Newtonsoft.Json.Linq.JObject.FromObject(jObject);
            LogType = logType;
            Settings = settings;
            Instant = SystemClock.Instance.GetCurrentInstant();
            StackTrace = new StackTrace(true);
        }
    }
}