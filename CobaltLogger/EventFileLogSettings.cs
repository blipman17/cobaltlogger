﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace CobaltLogger
{
    public class EventFileLogSettings : FileLogSettings
    {
        private readonly string directory;

        [JsonIgnore]
        public override string LogFile => directory + EventFileName();

        public EventFileLogSettings(string directory, LogType[] logTypes, bool printStackTrace, bool showUtcTime, bool showLocalTime, bool printObject)
            : base(directory, logTypes, printStackTrace, showUtcTime, showLocalTime, printObject)
        {
            this.directory = directory;
        }

        private static string EventFileName()
        {
            var zdt = LogSettings.GetCurrentZoneDateTime();
            var currentTime = zdt.ToString(
                "uuuu-MM-ddTHH:mm:ss;FFFFFFFFF z (o<g>)", CultureInfo.InvariantCulture);
            var fileName = $"{currentTime}.Log".RemoveIllegalFileCharacters();
            return fileName;
        }
    }
}